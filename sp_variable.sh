#!/bin/sh
a=10
echo -e "Value of a is $a \n"
echo "Value of a is ${a}\n"
DATE=`date`
echo "Date is $DATE"
USERS=`who | wc -l`
echo "Logged in user are $USERS"
UP=`data ; uptime`
echo "Uptime is $UP"
echo "**************"

echo ${var:-"Variable is not set"}
echo "1 - Value of var is ${var}"

echo ${var:="Variable is not set"}
echo "2 - Value of var is ${var}"

unset var
echo ${var:+"This is default value"}
echo "3 - Value of var is $var"

var="Prefix"
echo ${var:+"This is default value"}
echo "4 - Value of var is $var"

unset var
echo ${var:?"Print this message in error output"}
#:? is often used for check if the var was given value 
echo "5 - Value of var is ${var}"